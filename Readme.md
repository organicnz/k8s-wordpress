## Creating A Wordpress application using Kubernetes

Autor [Vivek Sonar](https://medium.com/@vivekksonar2000/creating-a-wordpress-application-using-kubernetes-fb6827f4445c)

    Objectives:
    1: Create a three-node Kubernetes cluster
    2: Create Dynamic, persistent volume claim
    3: Deploy MySQL and WordPress Pods
    4: Test Data Persistent on Pod Failure

    Things to keep in mind
    1: Deployment are designed for stateless applications
    2: When using persistent disk you can only run a single pod

![alt text](images/1Main.png "Main Image")

To get started we will first create a Directory for our project to work

```sh
$ mkdir k8s-wordpress && cd k8s-wordpress
```

Now we will require some storage to work with so we will create Persistent volume claim for our MySQL pod
create a mysql-volumeclaim.yaml file for it

```sh
kind: PersitentVolumeClaim
apiVersion: v1
metadata:
  name: mysql-volumeclaim
spec:
  accessMode:
    - ReadWriteOnce
  resources:
    requests:
      storage: 30Gi
```

Here we are just making a claim for 200GB Volume in Read-write mode. The volume will be provisioned first, and then it will be claimed by our MySQL pod

Now we can create mysql-deployment.yaml  file for our MySQL pod

```sh
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mysql
  labels:
    app: mysql
spec:
  replicas: 1
  selector:
    matchLabels:
      app: mysql
  template:
    metadata:
      labels:
        app: mysql
    spec:
      containers:
        - image: mysql:5.6
          name: mysql
          env:
            - name: MYSQL_ROOT_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: mysql
                  key: password
          ports:
            - containerPort: 3306
              name: mysql
          volumeMounts:
            - name: mysql-persistent-storage
              mountPath: /var/lib/mysql
      volumes:
        - name: mysql-persistent-storage
          persistentVolumeClaim:
            claimName: mysql-volumeclaim
```

Note: Here we are only creating a single replica, so we don’t have any issue with our read-write volume.

We are passing a Environment variable in our MySQL container for its root password using a secret object.

Now, if you look at our mysql-deployment.yaml you can see (line 32–35) we are associating our persistent volume object with this deployment and then (line 29–31) mounting it inside the MySQL container.

Now we will create mysql-service.yaml
This will create an internal service to access our MySQL deployment

```sh
apiVersion: v1
kind: Service
metadata:
  name: mysql
  lables:
    app: mysql
spec:
type: ClusterIP
ports:
    - port: 3306
selector:
    app: mysql
```

We are done with MySQL now and can move forward with Wordpress deployments.

We will create a persistent volume claim for our Wordpress Application in wordpress-volumeclaim.yaml.

```sh
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: wordpress-volumeclaim
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 20Gi
```

Similar to this we will create a wordpress-deployment.yaml for Wordpress application.

```sh
apiVersion: apps/v1
kind: Deployment
metadata:
  name: wordpress
  labels:
    app: wordpress
spec:
  replicas: 1
  selector:
    matchLabels:
      app: wordpress
  template:
    metadata:
      labels:
        app: wordpress
    spec:
      containers:
        - image: wordpress
          name: wordpress
          env:
          - name: WORDPRESS_DB_HOST
            value: mysql:3306
          - name: WORDPRESS_DB_PASSWORD
            valueFrom:
              secretKeyRef:
                name: mysql
                key: password
          ports:
            - containerPort: 80
              name: wordpress
          volumeMounts:
            - name: wordpress-persistent-storage
              mountPath: /var/www/html
      volumes:
        - name: wordpress-persistent-storage
          persistentVolumeClaim:
            claimName: wordpress-volumeclaim
```

And for our last manifest wordpress-service.yaml  file, we will create a service definition to expose our Wordpress Application for the outside world.

```sh
apiVersion: v1
kind: Service
metadata:
  labels:
    app: wordpress
  name: wordpress
spec:
  type: LoadBalancer
  ports:
    - port: 80
      targetPort: 80
      protocol: TCP
  selector:
    app: wordpress
```

Note: Assuming you are doing this project on a public cloud we are selecting type as a LoadBalancer.

Now we have everything for our application now we will simply kubectl apply our files.
So move to your terminal and do,

```sh
kubectl apply -f mysql-volumeclaim.yaml -f wordpress-volumeclaim.yaml
```

This will provision our persistent volumes for us. Wait for a moment unlit the volume are up. You can check it by

```sh
$ kubectl get pvc
```

Now we need to create that secret object we reference to store MySQL password.

```sh
$ kubectl create secret generic mysql --from-literal=password=YOURPASSWORD 
```

Now we are ready to deploy MySQL workloads

```sh
$ kubectl apply -f mysql-deployment.yaml -f mysql-service.yaml
```

Wait for a few moments and check the status of our pods with

```sh
$ kubectl get pods
```

Check if service is running properly

```sh
$ kubectl get svc
```

If it's running properly we can proceed to our Wordpress workloads.

```sh
$ kubectl apply -f wordpress-deployment.yaml -f wordpress-service.yaml
```

Wait for some time and check the status of the pods with

```sh
$ kubectl get pods
```

Check if the LoadBalancer is set up properly or not

```sh
$ kubectl get svc
```

You will get public IP for your Wordpress blog copy it and past it in a new tab of browser.And you get the Wordpress initial setup tour something like this.

![alt text](images/2Wordpress.png "First Wordpress Image")

![alt text](images/3Wordpress.png "First Wordpress Image")

Complete the setup, set a password and you have yourself a Wordpress blog running in Kubernetes. Select your them and get going.